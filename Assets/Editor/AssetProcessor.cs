using Object = System.Object;
using UnityEngine;
using UnityEditor;

public class AssetProcessor : AssetPostprocessor
{
    void OnPostprocessGameObjectWithUserProperties(
        GameObject go,
        string[] propNames,
        System.Object[] values)
    {
        for (int i = 0; i < propNames.Length; i++)
        {
            var propName = propNames[i];
            var value = (Object)values[i];

            //Debug.Log($"Propname: {propName}, Value: {values[i]}");

            var valString = value.ToString();
            switch (valString)
            {
                case "0":
                    go.AddComponent<BoxCollider>();
                    //Debug.Log($"adding BoxCollider for {go.name}");
                    break;
                case "1":
                    go.AddComponent<SphereCollider>();
                    break;
                case "2":
                    go.AddComponent<CapsuleCollider>();
                    break;
                case "3":
                    go.AddComponent<MeshCollider>();
                    break;
                default:
                    break;
            }
            go.GetComponent<Renderer>().enabled = false;
            //GameObject.DestroyImmediate(go.GetComponent<MeshFilter>());
            //GameObject.DestroyImmediate(go.GetComponent<Renderer>());
        }
    }
}
