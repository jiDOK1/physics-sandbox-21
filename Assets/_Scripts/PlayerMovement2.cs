using UnityEngine;

public class PlayerMovementSimple : MonoBehaviour
{
    public float Thrust = 20f;
    public float RotSpeed = 100f;
    Rigidbody rb;
    Vector3 rotVector;
    Vector3 moveVector;
    Quaternion deltaRot;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rotVector = new Vector3(0, 0f, RotSpeed);
        moveVector = new Vector3(0f, 1f, 0f);
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.W))
        {
            rb.AddForce(transform.up * Thrust * 1000f * Time.deltaTime, ForceMode.Force);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            deltaRot = Quaternion.Euler(rotVector * Time.deltaTime);
            rb.MoveRotation(rb.rotation * deltaRot);
        }

        if (Input.GetKey(KeyCode.E))
        {
            deltaRot = Quaternion.Euler(-rotVector * Time.deltaTime);
            rb.MoveRotation(rb.rotation * deltaRot);
        }

    }
}
