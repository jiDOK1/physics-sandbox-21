using UnityEngine;

public class KinematicRbController : MonoBehaviour
{
    Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        //transform.position += Time.deltaTime * 3f * Vector3.up;
        rb.MovePosition(rb.position + Vector3.up * 10f * Time.deltaTime);
    }
}
