using UnityEngine;

[System.Serializable]
public struct Rectangle
{
    public Vector2 Center;
    public Vector2 HalfExtend;
}
