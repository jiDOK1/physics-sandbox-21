using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
    [SerializeField] Texture2D levelTex;
    [SerializeField] Vector2Int res;
    [SerializeField] GameObject[] blocks;
    Color32[] colors;

    void Awake()
    {
        res = new Vector2Int(levelTex.width, levelTex.height);
        colors = levelTex.GetPixels32();
        for (int y = 0; y < res.y; y++)
        {
            for (int x = 0; x < res.x; x++)
            {
                int curIdx = y * res.x + x;
                Color32 curCol = colors[curIdx];
                Vector3 curColV3 = new Vector3(curCol.r, curCol.g, curCol.b);
                GameObject curPrefab = null;
                Debug.Log(curColV3.ToString());
                if(curColV3 == new Vector3(0, 0, 0))
                {
                    continue;
                }
                else if (curColV3 == new Vector3(255, 106, 0))
                {
                    curPrefab = blocks[(int)BlockType.Normal];
                }
                else if (curColV3 == new Vector3(255, 0, 0))
                {
                    curPrefab = blocks[(int)BlockType.Bounce];
                }
                else if (curColV3 == new Vector3(178, 255, 0))
                {
                    curPrefab = blocks[(int)BlockType.Quicksand];
                }
                var block = Instantiate<GameObject>(curPrefab, new Vector3(x,y, 0), Quaternion.identity);
                block.transform.parent = transform;
            }
        }
    }
}

public enum BlockType
{
    Normal, Bounce, Quicksand
}
