using UnityEngine;

// TODO: accumulate positionchange from special - apply after collision resolution ???
public class Player : MonoBehaviour
{
    [SerializeField] float jumpSpeed = 9f;
    [SerializeField] float speed = 20f;
    [SerializeField] float friction = 3f;
    [SerializeField] float gravity = 9.81f;
    [SerializeField] int numJumps = 2;

    Block[] blocks;
    RectCollider col;
    Vector2 velocity;
    bool isGrounded, isJumpPressed;
    int jumpCounter;
    float xInput, yInput;

    void Start()
    {
        col = GetComponent<RectCollider>();
        GameObject blockParent = GameObject.Find("Blocks");
        blocks = blockParent.GetComponentsInChildren<Block>();
    }

    void Update()
    {
        isJumpPressed = false;
        if (Input.GetButtonDown("Jump"))
        {
            isJumpPressed = true;
        }
        xInput = Input.GetAxis("Horizontal");
        yInput = Input.GetAxis("Vertical");
    //}

    //void FixedUpdate()
    //{
        // x movement & collision
        velocity.x -= friction * Time.deltaTime * velocity.x;
        velocity.x += xInput * speed * Time.deltaTime;
        velocity.x = Mathf.Clamp(velocity.x, -15f, 15f);
        transform.position += Vector3.right * velocity.x * Time.deltaTime;
        for (int i = 0; i < blocks.Length; i++)
        {
            if (col.CollidesWith(blocks[i].RectCol))
            {
                float xDepth = col.GetXDepth(blocks[i].RectCol);
                float sign = Mathf.Sign(velocity.x);
                transform.position -= Vector3.right * (xDepth + 0.0001f) * sign;
            }
        }
        // jumping
        if (isJumpPressed && (isGrounded || jumpCounter < numJumps))
        {
            isJumpPressed = false;
            isGrounded = false;
            velocity.y = jumpSpeed + jumpCounter * 1.1f;
            jumpCounter++;
        }
        // y movement & collision
        velocity.y -= gravity * Time.deltaTime;
        velocity.y = Mathf.Clamp(velocity.y, -20f, 20);
        transform.position += Vector3.up * velocity.y * Time.deltaTime;
        for (int j = 0; j < blocks.Length; j++)
        {
            if (col.CollidesWith(blocks[j].RectCol))
            {
                float sign = Mathf.Sign(velocity.y);
                if(velocity.y < 0f)
                {
                    isGrounded = true;
                    jumpCounter = 0;
                    velocity.y = 0f;
                }
                else
                {
                    velocity.y *= -1f;
                }
                float yDepth = col.GetYDepth(blocks[j].RectCol);
                transform.position -= Vector3.up * (yDepth + 0.001f) * sign;
                velocity += blocks[j].Special();
            }
        }
    }
}
