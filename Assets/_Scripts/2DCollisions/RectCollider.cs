using UnityEngine;

public class RectCollider : MonoBehaviour
{
    [SerializeField]
    Rectangle rect = new Rectangle()
    {
        Center = new Vector2(),
        HalfExtend = new Vector2(0.5f, 0.5f)
    };

    public bool CollidesWith(RectCollider other)
    {
        return GetXDepth(other) >= 0f && GetYDepth(other) >= 0f;
    }

    public float GetXDepth(RectCollider other)
    {
        float x1 = other.transform.position.x + other.rect.Center.x;
        float x2 = transform.position.x + rect.Center.x;
        return other.rect.HalfExtend.x + rect.HalfExtend.x - Mathf.Abs(x1 - x2);
    }

    public float GetYDepth(RectCollider other)
    {
        float y1 = other.transform.position.y + other.rect.Center.y;
        float y2 = transform.position.y + rect.Center.y;
        return other.rect.HalfExtend.y + rect.HalfExtend.y - Mathf.Abs(y1 - y2);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(
            transform.position + new Vector3(rect.Center.x, rect.Center.y),
            new Vector3(rect.HalfExtend.x * 2f, rect.HalfExtend.y * 2f));
    }
}
