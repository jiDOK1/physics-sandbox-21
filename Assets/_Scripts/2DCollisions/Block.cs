using UnityEngine;

public abstract class Block : MonoBehaviour
{
    public RectCollider RectCol;

    public abstract Vector2 Special();

    void Awake()
    {
        RectCol = GetComponent<RectCollider>();
    }
}
