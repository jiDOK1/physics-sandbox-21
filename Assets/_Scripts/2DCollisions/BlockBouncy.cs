using UnityEngine;

public class BlockBouncy : Block
{
    [SerializeField] float bouncePower = 1f;
    public override Vector2 Special()
    {
        return Vector2.up * bouncePower;
    }
}
