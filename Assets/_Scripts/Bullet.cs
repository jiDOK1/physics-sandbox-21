using UnityEngine;
using UnityEditor;

public class Bullet : MonoBehaviour
{
    public LayerMask mask;
    Vector3 lastPos;

    private void Awake()
    {
        lastPos = transform.position;
    }

    private void Update()
    {
        Vector3 castDir = lastPos - transform.position;
        Ray ray = new Ray(transform.position, castDir);
        RaycastHit hit = new RaycastHit();
        Debug.DrawRay(transform.position, castDir);
        if (Physics.Raycast(ray, out hit, castDir.magnitude, mask))
        {
            Pillar pillar = hit.collider.gameObject.GetComponent<Pillar>();
            if ( pillar != null)
            {
                pillar.DestroySelf(hit.point + castDir.normalized * 0.75f);
            }
            Destroy(gameObject);
        }
        lastPos = transform.position;
    }
}
