using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float Thrust = 20f;
    public float RotSpeed = 100f;
    Rigidbody rb;
    Vector3 rotVector;
    Vector3 moveVector;
    Quaternion deltaRot;
    float curThrust;
    float curRot;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        moveVector = new Vector3(0f, 1f, 0f);
    }

    void Update()
    {
        curThrust = Input.GetKey(KeyCode.W) ? 1f : 0f;

        if (Input.GetKey(KeyCode.Q))
        {
            rotVector = new Vector3(0f, 0f, RotSpeed);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            rotVector = new Vector3(0f, 0f, -RotSpeed);
        }
        else
        {
            rotVector = Vector3.zero;
        }
    }

    void FixedUpdate()
    {
        rb.AddForce(transform.up * Thrust * 1000f * Time.deltaTime * curThrust, ForceMode.Force);
        deltaRot = Quaternion.Euler(rotVector * Time.deltaTime);
        rb.MoveRotation(rb.rotation * deltaRot);
    }
}
