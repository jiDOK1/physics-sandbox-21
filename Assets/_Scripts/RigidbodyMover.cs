using UnityEngine;

public class RigidbodyMover : MonoBehaviour
{
    Rigidbody rb;
        
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        rb.velocity = transform.up * Time.fixedDeltaTime * 50f;
    }
}
