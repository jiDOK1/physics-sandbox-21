using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public GameObject BulletPrefab;
    public float Strength = 10f;
    Transform bulletSpawn;

    void Awake()
    {
        bulletSpawn = transform.Find("BulletSpawn");
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Rigidbody bullet = Instantiate<Rigidbody>(BulletPrefab.GetComponent<Rigidbody>(), bulletSpawn.position, Quaternion.identity);
            bullet.AddForce(bulletSpawn.forward * Strength, ForceMode.VelocityChange);
        }
    }
}
