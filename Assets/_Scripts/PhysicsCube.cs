using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsCube : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log($"GameObject {collision.gameObject.name} collided with me " +
            $"with a relative force of {collision.relativeVelocity.magnitude}!");
    }

    void OnCollisionExit(Collision collision)
    {
        
    }
}
