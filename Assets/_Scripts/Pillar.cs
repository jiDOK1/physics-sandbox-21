using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pillar : MonoBehaviour
{
    GameObject pillarDestroyed;
    Rigidbody rb;

    private void Awake()
    {
        pillarDestroyed = transform.Find("PillarDestroyed").gameObject;
    }

    public void DestroySelf(Vector3 pos)
    {
        gameObject.SetActive(false);
        pillarDestroyed.transform.parent = null;
        pillarDestroyed.SetActive(true);
        foreach (Transform child in pillarDestroyed.transform)
        {
            child.GetComponent<Rigidbody>().AddExplosionForce(500f, pos, 20f);
        }
    }
}
